package Pages.Page;

import Pages.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class Shopping extends TestBase {
    @FindBy(className = "app_logo")
    WebElement HomePageLogo;

    @FindBy(className = "product_label")
    WebElement HomePageProductText;

    @FindBy (xpath = "//*[name()='path' and contains(@fill,'currentCol')]")
    WebElement ShoppingCartButton;

    public Shopping(){
        PageFactory.initElements(driver, this);
    }
    //actions
    public boolean validateHomePageLogo(){
        return HomePageLogo.isDisplayed();
    }
    public String validateProductText(){
        return HomePageProductText.getText();
    }
    public void AddtoCartItemviaProductView(String ItemtoAdd) {
       String parentPath = "(//div[@class='inventory_item'])";
       List<WebElement> items = driver.findElements(By.xpath(parentPath));
           int click = 1;
           for (int i = 1; i <= items.size(); i++) {
           String parentPathAttriutes = "(//div[@class='inventory_item'])["+i+"]";
           String parentNodeText = driver.findElement(By.xpath(parentPathAttriutes)).getText();
           String ItemNamePath = "(//div[@class='inventory_item_name'])["+i+"]";
           String addCartButton = "(//button[@class='btn_primary btn_inventory'])["+click+"]";
           String itemName = driver.findElement(By.xpath(ItemNamePath)).getText();
           String cartButtonName = getElement(parentNodeText,3);
               if ( ItemtoAdd.equalsIgnoreCase(itemName) && cartButtonName.equalsIgnoreCase("ADD TO CART")) {
                   driver.findElement(By.xpath(addCartButton)).click();
                   break;
               }
               if (cartButtonName.equalsIgnoreCase("ADD TO CART"))
                   click++;
           }
    }
    public ShoppingCart clickshoppingCartButton(){
        ShoppingCartButton.click();
        return new ShoppingCart();
    }
    private String getElement(String nodeElement, int returnIndex){
        String [] parseValue = nodeElement.split("\n");
        return parseValue[returnIndex];
    }

}

