package Pages.Page;

import Pages.Base.TestBase;
import StepsDefination.ShoppingPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login extends TestBase {

    @FindBy(id = "user-name")
    WebElement username;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(xpath = "//div[@class='form_column']/following-sibling::div[1]")
    WebElement appsLogo;

    @FindBy (xpath = "//input[@type='submit']")
    WebElement loginButton;

    @FindBy (tagName = "h3")
    WebElement errorMessage;

    //initialize the page objects
    public Login (){
        PageFactory.initElements(driver, this);
    }

    //actions
    public String LoginPageTitle(){
        return driver.getTitle();
    }
    public boolean WebAppLogo(){
        return appsLogo.isDisplayed();
   }
    public String ErrorMessage(){
        return errorMessage.getText();
    }
    public void LoginDetails(String uname, String pword){
        username.sendKeys(uname);
        password.sendKeys(pword);
    }

    public Shopping clickLoginButton() {
        loginButton.click();
        return new Shopping();
    }
}
