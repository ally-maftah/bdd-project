package Pages.Page;

import Pages.Base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOut extends TestBase {

    @FindBy(xpath = "//a[@class='btn_action cart_button']")
    WebElement FinishButton;

    public  CheckOut(){
        PageFactory.initElements(driver,this);
    }

    public ThankYou clickFinishButton() {
        FinishButton.click();
        return new ThankYou();
    }
}