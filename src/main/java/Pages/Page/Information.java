package Pages.Page;

import Pages.Base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Information extends TestBase {
    @FindBy(id = "first-name")
    WebElement BuyerFirstName;

    @FindBy(id = "last-name")
    WebElement BuyersLastName;

    @FindBy(id = "postal-code")
    WebElement BuyersPostalCode;

    @FindBy(xpath = "(//input[@class='btn_primary cart_button'])")
    WebElement ContinueButton;

    public Information (){
        PageFactory.initElements(driver, this);
    }

    public void enterBuyersInformation(String fName, String lName, String zipCode){
        BuyerFirstName.sendKeys(fName);
        BuyersLastName.sendKeys(lName);
        BuyersPostalCode.sendKeys(zipCode);
    }
    public CheckOut clickContinueButton(){
        ContinueButton.click();
        return new CheckOut();
    }


}
