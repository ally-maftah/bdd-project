package Pages.Page;

import Pages.Base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ThankYou extends TestBase {
    @FindBy(xpath = "//div[text()='Finish']")
    WebElement FinishText;

    @FindBy(className = "complete-header")
    WebElement ThankYouOrderText;

    @FindBy (className = "pony_express")
    WebElement FinishLogo;

    public ThankYou(){
        PageFactory.initElements(driver,this);
    }
    public String verifyFinishText(){
        return FinishText.getText();
    }
    public String thankYouOrderText(){
        return ThankYouOrderText.getText();
    }
    public boolean checkThankYouLogo(){
        return FinishLogo.isDisplayed();
    }

}
