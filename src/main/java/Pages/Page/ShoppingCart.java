package Pages.Page;

import Pages.Base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCart extends TestBase {

    @FindBy(xpath = "//a[@class='btn_action checkout_button']")
    WebElement CheckOutButton;

    public ShoppingCart() {
        PageFactory.initElements(driver,this);
    }

    public Information clickCheckOutButton(){
        CheckOutButton.click();
        return new Information();
    }

}
