package StepsDefination;

import Pages.Base.TestBase;
import Pages.Page.Login;
import Pages.Page.Shopping;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;

public class LoginPage extends TestBase {
    Login login;
    Shopping shopping;

    @Given("^Open Browser$")
    public void open_Browser() {
        TestBase.initialize();
        login = new Login();
        Assert.assertEquals("Swag Labs",login.LoginPageTitle());
        Assert.assertTrue(login.WebAppLogo());
    }
    @Then("^User enter valid \\\"(.*)\\\" and \\\"(.*)\\\"$")
    public void user_enter_valid_username_and_password(String Username, String Password) {
        login.LoginDetails(Username, Password);
    }

    @Then("^user enter blocked \\\"(.*)\\\" and \\\"(.*)\\\"$")
    public void user_enter_blocked_username_and_password(String Username, String Password) {
        login.LoginDetails(Username, Password);
        login.clickLoginButton();
        Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.", login.ErrorMessage());
    }

    @Then("^User click login button$")
    public void user_click_login_button() {
        login.clickLoginButton();
    }

    @Then("^verify that user is able to login successfully$")
    public void verify_that_user_is_able_to_login_successfully() {
        shopping = new Shopping();
        Assert.assertTrue(shopping.validateHomePageLogo());
        Assert.assertEquals(shopping.validateProductText(),"Products");
    }

}
