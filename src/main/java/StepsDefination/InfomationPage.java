package StepsDefination;

import Pages.Page.Information;
import cucumber.api.java.en.Then;

public class InfomationPage {
    Information information = new Information();

    @Then("^user enter \\\"(.*)\\\" and \\\"(.*)\\\" and \\\"(.*)\\\"$")
    public void user_enter_FirstName_and_LastName_and_ZipCode(String FirstName, String LastName, String ZipCode){
        information.enterBuyersInformation(FirstName, LastName, ZipCode);
    }
    @Then("^user click the continue button$")
    public void user_click_the_continue_button(){
        information.clickContinueButton();
    }
}
